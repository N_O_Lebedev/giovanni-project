# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 14:18:42 2022

@author: Giovanni.Landriscina
"""
from person import Person

class Student(Person):
    def __init__(self,name, last_name):
        super().__init__(name,last_name)
        self.workshop= None
    def enroll(self,workshop):
        self.workshop = workshop
    def __str__(self):
        return(f'student {self.name} {self.last_name}')

if __name__ == '__main__':
    me = Student('Giovanni','Landriscina')
    print(me.workshop)
    me.enroll('Python for the Lab')
    print(me.workshop)
    print(me)